# Frent Docs

Документація сервісу оренди друзів **Frent** на основі генератора статичних сайтів [mkdocs](https://www.mkdocs.org/).

[Картка з вимогами](https://gitlab.com/reflex-lnu/frent/frent-docs/-/issues/1)

## Frent Deployment
Для розгортання сервісу локально необхідно `docker` i `docker-compose`. 

Змінити порти і пароль постгресу можна в файлі `.env`

Розгорнути сервіс локально:
```sh
git clone https://gitlab.com/reflex-lnu/frent/frent-client.git

cd frent-docs

docker-compose --env-file .env up
```
Клієнтський застосунок доступний за адресою [localhost:8080] (або `localhost:CLIENT_PORT`, де значення `CLIENT_PORT` з `.env` файлу)

API доступне за адресою [localhost:3000] (або `localhost:API_PORT` з `.env` файлу)

Постгрес сервер доступний на `5000` порті (або `PG_PORT` з `.env`)

Згорнути сервіc:
```sh
docker-compose -env-file .env down
```

Очистити збережений стан бази:
```
sudo rm -rf postgres
```


## Docs Local Deployment

1. `pip install pipenv` (лише першого разу)
2. `pipenv install`
3. `pipenv run mkdocs serve`
4. Локально розгорнутий сервер документації доступний за посиланням <http://127.0.0.1:8000/>.

> 💡 `Note`  
> Зміна пов'язаних файлів автоматично відобразиться і на сторінці, тобто немає потреби перезапускати сервер після кожної зміни, це робиться автоматично.

## GitLab Pages Deployment

Для застосування змін до опублікованого сайту на GitLab Pages:

1. Запушити зміни в гілку `main` (напряму або через **Merge Request**);
2. На GitLab зліва в меню в `CI/CD -> Pipelines`;
3. В колонці `Stages` вибрати коло з `deploy`;
4. В випадайці натиснути **▶** біля `pages`;
5. Дочекатись завершення виконання задачі;
6. Перевірити, чи застосувались зміни за посиланням <https://reflex-lnu.gitlab.io/frent/frent-docs/>

> ⚠️ `Warning`  
> Задача `pages` вимагає ранера з тегом `docker`.
> Наразі таким єдиним ранером є [`nebelfox-linux-docker-executor`][runner] з групи [`frent`][group].
> Стосовно стану і доступності ранера звертатися до @nebelfox.

[runner]: https://gitlab.com/groups/reflex-lnu/frent/-/runners/18397535
[group]: https://gitlab.com/groups/reflex-lnu/frent/-/runners
